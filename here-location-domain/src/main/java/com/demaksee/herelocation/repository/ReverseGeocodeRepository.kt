package com.demaksee.herelocation.repository

import androidx.lifecycle.LiveData
import com.demaksee.herelocation.models.AddressDetails
import com.demaksee.herelocation.models.Location

interface ReverseGeocodeRepository {

    fun reversGeocode(location: Location): LiveData<AddressDetails>

    fun reversStatus(): LiveData<Result<Any>>
}