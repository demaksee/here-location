package com.demaksee.herelocation.repository

import androidx.lifecycle.LiveData
import com.demaksee.herelocation.models.Category
import com.demaksee.herelocation.models.Location
import com.demaksee.herelocation.models.Place

interface PlacesRepository {

    fun getCategories(location: Location): LiveData<Result<List<Category>>>

    fun discoverPlaces(location: Location, categories: Set<Category>): LiveData<Result<List<Place>>>
}