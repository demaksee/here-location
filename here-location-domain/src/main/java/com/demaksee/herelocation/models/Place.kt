package com.demaksee.herelocation.models

data class Place(
    val id: String,
    val position: Location,
    val title: String,
    val category: Category,
    val distance: Int,
    val openingHours: OpeningHours
)

data class OpeningHours(val isOpen: Boolean)