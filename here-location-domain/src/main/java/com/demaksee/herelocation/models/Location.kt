package com.demaksee.herelocation.models

data class Location(val latitude: Double, val longitude: Double)