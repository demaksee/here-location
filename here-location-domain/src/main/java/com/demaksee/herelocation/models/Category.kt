package com.demaksee.herelocation.models

data class Category(val id: String, val title: String, val icon: String?)