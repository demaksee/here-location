package com.demaksee.herelocation.usecase

import androidx.lifecycle.LiveData
import com.demaksee.herelocation.models.AddressDetails
import com.demaksee.herelocation.models.Location
import com.demaksee.herelocation.repository.ReverseGeocodeRepository

class ReverseGeocodeUseCase(private val reverseGeocodeRepository: ReverseGeocodeRepository) {

    fun reverse(location: Location): LiveData<AddressDetails> =
        reverseGeocodeRepository.reversGeocode(location)

    fun reverseStatus(): LiveData<Result<Any>> =
        reverseGeocodeRepository.reversStatus()
}