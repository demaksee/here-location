package com.demaksee.herelocation.usecase

import androidx.lifecycle.LiveData
import com.demaksee.herelocation.models.Category
import com.demaksee.herelocation.models.Location
import com.demaksee.herelocation.models.Place
import com.demaksee.herelocation.repository.PlacesRepository

class PlacesUseCase(private val placesRepository: PlacesRepository) {

    fun getCategories(location: Location): LiveData<Result<List<Category>>> =
        placesRepository.getCategories(location)

    fun discoverPlaces(
        location: Location,
        categories: Set<Category>
    ): LiveData<Result<List<Place>>> =
        placesRepository.discoverPlaces(location, categories)
}