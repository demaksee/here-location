package com.demaksee.herelocation.networking

import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(val appId: String, val appCode: String): Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val newHttpUrl = request.url.newBuilder()
            .addQueryParameter("app_id", appId)
            .addQueryParameter("app_code", appCode)
            .build()
        val newRequest = request.newBuilder().url(newHttpUrl).build()
        return chain.proceed(newRequest)
    }
}