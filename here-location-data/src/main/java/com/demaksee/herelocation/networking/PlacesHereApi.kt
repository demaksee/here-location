package com.demaksee.herelocation.networking

import com.demaksee.herelocation.model.CategoriesResponse
import com.demaksee.herelocation.model.DiscoverResponse
import com.demaksee.herelocation.model.Proximity
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface PlacesHereApi {

    @GET("/places/v1/categories/places")
    fun getCategories(@Query("at") prox: Proximity): Call<CategoriesResponse>

    @GET("/places/v1/discover/explore")
    fun discoverPlaces(@Query("at") prox: Proximity, @Query("cat") catIds: String): Call<DiscoverResponse>

    companion object {

        private const val PLACES_BASE_URL = "https://places.demo.api.here.com"

        fun create(client: OkHttpClient, gson: Gson): PlacesHereApi {
            val retrofit = Retrofit.Builder()
                .baseUrl(PLACES_BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
            return retrofit.create<PlacesHereApi>(PlacesHereApi::class.java)
        }
    }
}