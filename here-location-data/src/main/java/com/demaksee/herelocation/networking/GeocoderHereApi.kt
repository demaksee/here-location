package com.demaksee.herelocation.networking

import com.demaksee.herelocation.model.AddressDetailsResponse
import com.demaksee.herelocation.model.Proximity
import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface GeocoderHereApi {

    @GET("/6.2/reversegeocode.json?mode=retrieveAddresses&maxresults=1&gen=9")
    fun reversGeocode(@Query("prox") prox: Proximity): Call<AddressDetailsResponse>

    companion object {

        private const val REVERSE_GEOCODER_BASE_URL = "https://reverse.geocoder.api.here.com"

        fun create(client: OkHttpClient, gson: Gson): GeocoderHereApi {
            val retrofit = Retrofit.Builder()
                .baseUrl(REVERSE_GEOCODER_BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
            return retrofit.create<GeocoderHereApi>(GeocoderHereApi::class.java)
        }
    }
}