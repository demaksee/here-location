package com.demaksee.herelocation.networking

import com.demaksee.herelocation.repository.PlacesRepositoryImpl
import com.demaksee.herelocation.repository.ReverseGeocodeRepository
import com.demaksee.herelocation.repository.ReverseGeocodeRepositoryImpl
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

object Networking {

    lateinit var reverseGeocodeRepositoryImpl: ReverseGeocodeRepository

    lateinit var placesRepositoryImpl: PlacesRepositoryImpl

    fun initialize(appId: String, appCode: String) {
        val okHttpClient = createOkHttpClient(appId, appCode)
        val geocoderHereApi = GeocoderHereApi.create(okHttpClient, createGsonForGeocoder())
        val placesHereApi = PlacesHereApi.create(okHttpClient, createGsonForPlaces())

        reverseGeocodeRepositoryImpl = ReverseGeocodeRepositoryImpl(geocoderHereApi)
        placesRepositoryImpl = PlacesRepositoryImpl(placesHereApi)
    }

    private fun createOkHttpClient(appId: String, appCode: String): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            .addNetworkInterceptor(AuthInterceptor(appId, appCode))
            .build()


    private fun createGsonForGeocoder() = GsonBuilder()
        .disableHtmlEscaping()
        .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
        .setPrettyPrinting()
        .serializeNulls()
        .create()

    private fun createGsonForPlaces() = GsonBuilder()
        .disableHtmlEscaping()
        .setPrettyPrinting()
        .serializeNulls()
        .create()
}