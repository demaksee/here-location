package com.demaksee.herelocation.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.demaksee.herelocation.model.CategoriesResponse
import com.demaksee.herelocation.model.DiscoverResponse
import com.demaksee.herelocation.model.Proximity
import com.demaksee.herelocation.models.Category
import com.demaksee.herelocation.models.Location
import com.demaksee.herelocation.models.Place
import com.demaksee.herelocation.networking.PlacesHereApi
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PlacesRepositoryImpl(private val placesHereApi: PlacesHereApi) :
    PlacesRepository {

    override fun getCategories(location: Location): LiveData<Result<List<Category>>> {
        val categories: MutableLiveData<Result<List<Category>>> = MutableLiveData()
        val call = placesHereApi.getCategories(Proximity(location.latitude, location.longitude))

        call.enqueue(object : Callback<CategoriesResponse> {
            override fun onFailure(call: Call<CategoriesResponse>, t: Throwable) {
                categories.value = Result.failure(t)
            }

            override fun onResponse(
                call: Call<CategoriesResponse>,
                response: Response<CategoriesResponse>
            ) {
                val categoriesResponse: CategoriesResponse? = response.body()
                val list = categoriesResponse?.items?.map(::convert)
                if (list != null) {
                    categories.value = Result.success(list)
                } else {
                    categories.value = Result.failure(IllegalStateException("no result"))
                }
            }
        })
        return categories
    }

    override fun discoverPlaces(
        location: Location,
        categories: Set<Category>
    ): LiveData<Result<List<Place>>> {
        val places: MutableLiveData<Result<List<Place>>> = MutableLiveData()
        val catIds = categories.map { it.id }.joinToString(separator = ",")
        val call =
            placesHereApi.discoverPlaces(Proximity(location.latitude, location.longitude), catIds)

        call.enqueue(object : Callback<DiscoverResponse> {
            override fun onFailure(call: Call<DiscoverResponse>, t: Throwable) {
                places.value = Result.failure(t)
            }

            override fun onResponse(
                call: Call<DiscoverResponse>,
                response: Response<DiscoverResponse>
            ) {
                val discoverResponse: DiscoverResponse? = response.body()
                val list = discoverResponse?.results?.items?.map(::convert)
                if (list != null) {
                    places.value = Result.success(list)
                } else {
                    places.value = Result.failure(IllegalStateException("no result"))
                }
            }
        })
        return places
    }
}