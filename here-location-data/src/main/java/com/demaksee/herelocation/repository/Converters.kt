package com.demaksee.herelocation.repository

import com.demaksee.herelocation.models.Category
import com.demaksee.herelocation.models.Location
import com.demaksee.herelocation.models.OpeningHours
import com.demaksee.herelocation.models.Place

fun convert(category: com.demaksee.herelocation.model.Category) = with(category) {
    Category(id, title, icon)
}

fun convert(place: com.demaksee.herelocation.model.Place) = with(place) {
    Place(
        id,
        Location(position[0], position[1]),
        title,
        convert(category),
        distance,
        OpeningHours(openingHours?.isOpen ?: false)
    )
}