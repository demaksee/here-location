package com.demaksee.herelocation.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.demaksee.herelocation.models.AddressDetails
import com.demaksee.herelocation.models.Location
import com.demaksee.herelocation.networking.GeocoderHereApi
import com.demaksee.herelocation.model.AddressDetailsResponse
import com.demaksee.herelocation.model.Proximity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.IllegalStateException

class ReverseGeocodeRepositoryImpl(private val geocoderHereApi: GeocoderHereApi) :
    ReverseGeocodeRepository {

    private val reversStatus = MutableLiveData<Result<Any>>()

    override fun reversGeocode(location: Location): LiveData<AddressDetails> {
        val addressLiveData: MutableLiveData<AddressDetails> = MutableLiveData()

        val call =
            geocoderHereApi.reversGeocode(Proximity(location.latitude, location.longitude, 250))

        call.enqueue(object : Callback<AddressDetailsResponse> {
            override fun onFailure(call: Call<AddressDetailsResponse>, t: Throwable) {
                reversStatus.value = Result.failure(t)
            }

            override fun onResponse(
                call: Call<AddressDetailsResponse>,
                response: Response<AddressDetailsResponse>
            ) {
                val addressDetails = response.body()
                val label =
                    addressDetails?.response?.view?.get(0)?.result?.get(0)?.location?.address?.label
                if (label != null) {
                    addressLiveData.postValue(AddressDetails(label))
                    reversStatus.value = Result.success(Any())
                } else {
                    reversStatus.value = Result.failure(IllegalStateException("empty result"))
                }
            }
        })
        return addressLiveData
    }

    override fun reversStatus(): LiveData<Result<Any>> {
        return reversStatus
    }
}