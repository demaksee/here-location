package com.demaksee.herelocation.model

data class DiscoverResponse(val results: Results)

data class Results(
    val next: String,
    val items: List<Place>
)

data class Place(
    val id: String,
    val position: DoubleArray,
    val title: String,
    val category: Category,
    val distance: Int,
    val openingHours: OpeningHours?
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Place

        if (id != other.id) return false
        if (!position.contentEquals(other.position)) return false
        if (title != other.title) return false
        if (category != other.category) return false
        if (distance != other.distance) return false
        if (openingHours != other.openingHours) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + position.contentHashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + category.hashCode()
        result = 31 * result + distance
        result = 31 * result + (openingHours?.hashCode() ?: 0)
        return result
    }

}

data class OpeningHours(val isOpen: Boolean)