package com.demaksee.herelocation.model

data class AddressDetailsResponse(val response: Response)

data class Response(val view: List<View>)

data class View(val result: List<Result>)

data class Result(val location: Location)

data class Location (val address: Address)

data class Address(val label: String)
