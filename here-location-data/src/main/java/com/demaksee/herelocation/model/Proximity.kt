package com.demaksee.herelocation.model

data class Proximity(val latitude: Double, val longitude: Double, val radius: Int? = null) {

    override fun toString() =
        if (radius == null) {
            String.format("%.4f,%.4f", latitude, longitude)
        } else {
            String.format("%.4f,%.4f,%d", latitude, longitude, radius)
        }
}