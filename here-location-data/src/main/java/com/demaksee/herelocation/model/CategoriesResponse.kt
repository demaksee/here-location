package com.demaksee.herelocation.model

data class CategoriesResponse(val items: List<Category>)

data class Category(
    val id: String,
    val title: String,
    val icon: String,
    val type: String,
    val href: String,
    val system: String
)
