package com.demaksee.herelocation.com.demaksee.herelocation

import android.app.Application
import com.demaksee.herelocation.InjectorUtils
import com.demaksee.herelocation.R
import com.demaksee.herelocation.networking.Networking

class HereLocationApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Networking.initialize(
            getString(R.string.here_api_app_id),
            getString(R.string.here_api_app_code)
        )

        InjectorUtils.reverseGeocodeRepositoryProvider = {
            Networking.reverseGeocodeRepositoryImpl
        }

        InjectorUtils.placesRepositoryProvider = {
            Networking.placesRepositoryImpl
        }
    }
}