package com.demaksee.herelocation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.demaksee.herelocation.models.AddressDetails
import com.demaksee.herelocation.models.Category
import com.demaksee.herelocation.models.Location
import com.demaksee.herelocation.models.UiCategory
import com.demaksee.herelocation.usecase.PlacesUseCase
import com.demaksee.herelocation.usecase.ReverseGeocodeUseCase
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {

    @get:Rule // -> allows liveData to work on different thread besides main, must be public!
    var executorRule = InstantTaskExecutorRule()

    @Mock
    lateinit var reverseGeocodeUseCase: ReverseGeocodeUseCase

    @Mock
    lateinit var placesUseCase: PlacesUseCase

    @Mock
    lateinit var mockObserver: Observer<AddressDetails>

    @Mock
    lateinit var mockStatusObserver: Observer<Result<Any>>

    @Mock
    lateinit var mockCategoriesObserver: Observer<Result<List<UiCategory>>>

    lateinit var viewModel: MainViewModel

    private val location1 = Location(1.0, 2.0)

    private val category = Category("id", "title", null)

    private val uiCategory = UiCategory(category, false)

    @Before
    fun setUp() {
        doAnswer { MutableLiveData(Result.success(1)) }
            .`when`(reverseGeocodeUseCase).reverseStatus()
        doAnswer { MutableLiveData(AddressDetails(it.arguments[0].toString())) }
            .`when`(reverseGeocodeUseCase).reverse(location1)

        doAnswer { MutableLiveData(Result.success(listOf(category))) }
            .`when`(placesUseCase).getCategories(location1)

        viewModel = MainViewModel(reverseGeocodeUseCase, placesUseCase)
    }

    @Test
    fun testGetReversedGeocode() {
        viewModel.reversedGeocode.observeForever(mockObserver)
        viewModel.setLocation(location1)

        verify(mockObserver).onChanged(AddressDetails("Location(latitude=1.0, longitude=2.0)"))
        verifyNoMoreInteractions(mockObserver)
    }

    @Test
    fun testGetReversStatus() {
        viewModel.reversStatus.observeForever(mockStatusObserver)
        verify(mockStatusObserver).onChanged(Result.success(1))
        verifyNoMoreInteractions(mockStatusObserver)
    }

    @Test
    fun testSetLocation() {
        assertTrue(viewModel.setLocation(location1))
    }

    @Test
    fun testGetCategories() {
        viewModel.getCategories().observeForever(mockCategoriesObserver)
        viewModel.setLocation(location1)

        verify(mockCategoriesObserver).onChanged(Result.success(listOf(uiCategory)))
    }

    @Test
    fun testSelectCategory() {
        viewModel.selectCategory(category, true)
    }

    @Test
    fun testDeselectAllCategories() {
        assertFalse(viewModel.deselectAllCategories())
        viewModel.selectCategory(category, true)
        assertTrue(viewModel.deselectAllCategories())
    }
}