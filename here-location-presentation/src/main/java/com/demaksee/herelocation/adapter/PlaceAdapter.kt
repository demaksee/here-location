package com.demaksee.herelocation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.demaksee.herelocation.models.Place
import com.demaksee.herelocation.presentation.R
import com.demaksee.herelocation.presentation.databinding.ListItemPlaceBinding

class PlaceAdapter : ListAdapter<Place, PlaceAdapter.ViewHolder>(PlaceDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemPlaceBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val binding: ListItemPlaceBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(place: Place) {
            binding.title.text = place.title
            binding.category.text = place.category.title
            binding.distance.text = itemView.context.getString(R.string.distance, place.distance)
            binding.isOpen.setText(if (place.openingHours.isOpen) R.string.open else R.string.closed)
        }
    }
}

private class PlaceDiffCallback : DiffUtil.ItemCallback<Place>() {
    override fun areItemsTheSame(oldItem: Place, newItem: Place) =
        oldItem.category.id == newItem.category.id

    override fun areContentsTheSame(oldItem: Place, newItem: Place) =
        oldItem == newItem

}