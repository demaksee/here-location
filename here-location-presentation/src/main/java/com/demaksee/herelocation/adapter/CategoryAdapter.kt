package com.demaksee.herelocation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.demaksee.herelocation.models.UiCategory
import com.demaksee.herelocation.models.Category
import com.demaksee.herelocation.presentation.databinding.ListItemCategoryBinding

class CategoryAdapter :
    ListAdapter<UiCategory, CategoryAdapter.ViewHolder>(
        CategoryDiffCallback()
    ) {

    var onChangeListener: ((Category, Boolean) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemCategoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(private val binding: ListItemCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        private var currentCategory: Category? = null

        val listener = CompoundButton.OnCheckedChangeListener { _, checked ->
            currentCategory?.let { onChangeListener?.invoke(it, checked) }
        }

        init {
            binding.checkbox.setOnCheckedChangeListener(listener)
        }

        fun bind(category: UiCategory) {
            binding.categoryTitle.text = category.category.title
            binding.checkbox.setOnCheckedChangeListener(null)
            binding.checkbox.isChecked = category.checked
            binding.checkbox.setOnCheckedChangeListener(listener)
            currentCategory = category.category
            Glide.with(binding.icon)
                .load(category.category.icon)
                .fitCenter()
                .into(binding.icon)
        }
    }
}

private class CategoryDiffCallback : DiffUtil.ItemCallback<UiCategory>() {
    override fun areItemsTheSame(oldItem: UiCategory, newItem: UiCategory) =
        oldItem.category.id == newItem.category.id

    override fun areContentsTheSame(oldItem: UiCategory, newItem: UiCategory) =
        oldItem == newItem

}