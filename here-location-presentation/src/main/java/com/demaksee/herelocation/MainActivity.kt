package com.demaksee.herelocation

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.core.view.ViewCompat
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.demaksee.herelocation.adapter.PlaceAdapter
import com.demaksee.herelocation.models.Location
import com.demaksee.herelocation.models.Place
import com.demaksee.herelocation.presentation.R
import com.demaksee.herelocation.presentation.databinding.ActivityMainBinding
import com.demaksee.herelocation.viewmodel.MainViewModel
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar

class MainActivity : AppCompatActivity() {

    private lateinit var map: GoogleMap

    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(
            LayoutInflater.from(
                this
            )
        )
    }

    private val viewModel: MainViewModel by viewModels { InjectorUtils.provideMainViewModelFactory() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        ViewCompat.setOnApplyWindowInsetsListener(binding.root) { _, insets -> insets }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync { onMapReady(it) }

        // details here: https://stackoverflow.com/a/57557081/755313
        val locationButton =
            (mapFragment.view?.findViewById<View>(Integer.parseInt("1"))?.parent as View).findViewById<View>(
                Integer.parseInt("2")
            )
        val layoutParams = locationButton.layoutParams as RelativeLayout.LayoutParams
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0)
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE)
        val margin = resources.getDimensionPixelSize(R.dimen.my_location_button_margin)
        layoutParams.setMargins(0, 0, margin, margin)

        viewModel.reversedGeocode.observe(this) { addressDetails ->
            binding.address.text = addressDetails.label
        }

        viewModel.reversStatus.observe(this) { status ->
            if (status.isFailure) {
                Snackbar.make(
                    binding.root,
                    getString(R.string.address_detection_failed),
                    Snackbar.LENGTH_SHORT
                ).show()
                binding.address.text = ""
            }
            binding.progress.hide()
        }

        binding.nearby.setOnClickListener {
            CategoriesDialogFragment().show(supportFragmentManager, null)
        }

        val layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.places.layoutManager = layoutManager
        val placeAdapter = PlaceAdapter()
        binding.places.adapter = placeAdapter
        val snapHelper = PagerSnapHelper()
        snapHelper.attachToRecyclerView(binding.places)

        viewModel.discoverPlaces().observe(this) { result ->
            if (result.isFailure) {
                return@observe
            }
            val markerMap = mutableMapOf<Marker, Place>()
            val list = result.getOrDefault(listOf())
            map.clear()
            for (item in list) {
                val markerOptions = MarkerOptions()
                    .position(LatLng(item.position.latitude, item.position.longitude))
                    .title(item.title)
                val marker = map.addMarker(markerOptions)
                markerMap[marker] = item
            }

            map.setOnMarkerClickListener {
                binding.places.smoothScrollToPosition(list.indexOf(markerMap[it]))
                it.isInfoWindowShown
            }
            placeAdapter.submitList(list)
        }

        binding.progress.hide()
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     *
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    private fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        if (checkLocationService() && checkPermissions()) {
            startListeningMyLocation()
        }
    }

    override fun onResume() {
        super.onResume()
        if (::map.isInitialized) {
            // let's check, maybe user enabled location service and returned to the app
            if (checkLocationService() && checkPermissions()) {
                startListeningMyLocation()
            }
        }
    }

    private fun startListeningMyLocation() {
        with(map) {
            isMyLocationEnabled = true
            uiSettings.isMyLocationButtonEnabled = true

            // https://stackoverflow.com/questions/36444085/android-setonmylocationchangelistener-is-deprecated#comment66391578_36444144
            @Suppress("DEPRECATION")
            setOnMyLocationChangeListener {
                binding.address.text.ifEmpty { getString(R.string.loading) }
                if (viewModel.setLocation(Location(it.latitude, it.longitude))) {
                    binding.progress.show()
                }
            }
        }
    }

    override fun onBackPressed() {
        if (!viewModel.deselectAllCategories()) {
            super.onBackPressed()
        }
    }

    private fun checkLocationService(): Boolean {
        try {
            val lm = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                return true
            }
            AlertDialog.Builder(this)
                .setMessage(getString(R.string.location_service_dialog_message))
                .setPositiveButton(getString(R.string.location_service_dialog_positive)) { _, _ ->
                    startActivity(
                        Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    )
                }
                .setNegativeButton(R.string.location_dialog_cancel, null)
                .show()
        } catch (e: IllegalArgumentException) {
            //  there is no GPS, aborting
        }
        return false
    }

    private fun checkPermissions(): Boolean {
        val granted =
            checkSelfPermission(this, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
        if (!granted) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, ACCESS_FINE_LOCATION)) {
                AlertDialog.Builder(this)
                    .setMessage(R.string.location_dialog_message)
                    .setPositiveButton(R.string.location_dialog_allow) { _, _ -> requestPermissions() }
                    .setNegativeButton(R.string.location_dialog_cancel, null)
                    .show()
            } else {
                requestPermissions()
            }
        }
        return granted
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this, arrayOf(ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PERMISSION_GRANTED) {
                    startListeningMyLocation()
                }
            }
        }
    }

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }
}
