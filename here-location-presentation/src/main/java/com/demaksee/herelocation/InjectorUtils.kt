package com.demaksee.herelocation

import com.demaksee.herelocation.repository.PlacesRepository
import com.demaksee.herelocation.repository.ReverseGeocodeRepository
import com.demaksee.herelocation.usecase.PlacesUseCase
import com.demaksee.herelocation.usecase.ReverseGeocodeUseCase
import com.demaksee.herelocation.viewmodel.MainViewModelFactory

object InjectorUtils { // Dagger will be integrated later

    lateinit var reverseGeocodeRepositoryProvider: () -> ReverseGeocodeRepository
    lateinit var placesRepositoryProvider: () -> PlacesRepository

    fun provideMainViewModelFactory(): MainViewModelFactory =
        MainViewModelFactory(
            {
                ReverseGeocodeUseCase(reverseGeocodeRepositoryProvider())
            },
            {
                PlacesUseCase(placesRepositoryProvider())
            }
        )
}