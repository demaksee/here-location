package com.demaksee.herelocation.models

data class UiCategory(val category: Category, val checked: Boolean)