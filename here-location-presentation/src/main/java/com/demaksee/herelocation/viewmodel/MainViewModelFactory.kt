package com.demaksee.herelocation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.demaksee.herelocation.usecase.PlacesUseCase
import com.demaksee.herelocation.usecase.ReverseGeocodeUseCase

class MainViewModelFactory(
    private val reverseGeocodeUseCaseProvider: () -> ReverseGeocodeUseCase,
    private val placesUseCaseProvider: () -> PlacesUseCase
) :
    ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        return MainViewModel(
            reverseGeocodeUseCaseProvider(),
            placesUseCaseProvider()
        ) as T
    }
}