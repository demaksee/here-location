package com.demaksee.herelocation.viewmodel

import androidx.lifecycle.*
import com.demaksee.herelocation.models.AddressDetails
import com.demaksee.herelocation.models.Category
import com.demaksee.herelocation.models.Location
import com.demaksee.herelocation.models.UiCategory
import com.demaksee.herelocation.usecase.PlacesUseCase
import com.demaksee.herelocation.usecase.ReverseGeocodeUseCase

class MainViewModel(
    private val reverseGeocodeUseCase: ReverseGeocodeUseCase,
    private val placesUseCase: PlacesUseCase
) : ViewModel() {

    private val currentLocation = MutableLiveData<Location>()

    private val selectedCategories = mutableSetOf<Category>()

    private val selectedCategoriesLiveData = MutableLiveData<Set<Category>>()

    val reversedGeocode: LiveData<AddressDetails> = currentLocation.switchMap {
        reverseGeocodeUseCase.reverse(it)
    }

    val reversStatus = reverseGeocodeUseCase.reverseStatus()

    fun setLocation(location: Location): Boolean {
        val isNew = location.distance(currentLocation.value) > 5
        if (isNew) { // ignoring jitter
            currentLocation.value = location
        }
        return isNew
    }

    fun getCategories() = currentLocation.switchMap {
        placesUseCase.getCategories(it)
    }.map {
        it.map {
            it.map {
                UiCategory(
                    it,
                    selectedCategories.contains(it)
                )
            }
        }
    }

    fun discoverPlaces() =
        currentLocation.switchMap { location ->
            selectedCategoriesLiveData.switchMap { cats ->
                if (cats.isEmpty()) {
                    MutableLiveData(Result.success(listOf()))
                } else {
                    placesUseCase.discoverPlaces(location, cats)
                }
            }
        }

    fun selectCategory(category: Category, selected: Boolean) {
        if (selected) {
            selectedCategories.add(category)
        } else {
            selectedCategories.remove(category)
        }
        selectedCategoriesLiveData.postValue(selectedCategories)
    }

    /**
     * Return true if list was not empty
     */
    fun deselectAllCategories(): Boolean {
        val result = selectedCategories.isNotEmpty()
        selectedCategories.clear()
        selectedCategoriesLiveData.postValue(selectedCategories)
        return result
    }

    private fun Location.distance(location: Location?) =
        location?.let {
            val result = FloatArray(1)
            android.location.Location.distanceBetween(
                latitude, longitude,
                location.latitude, location.longitude, result
            )
            result[0]
        } ?: Float.MAX_VALUE
}