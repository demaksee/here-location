package com.demaksee.herelocation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.demaksee.herelocation.adapter.CategoryAdapter
import com.demaksee.herelocation.presentation.databinding.FragmentDialogCategoriesBinding
import com.demaksee.herelocation.viewmodel.MainViewModel
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class CategoriesDialogFragment : BottomSheetDialogFragment() {

    private lateinit var binding: FragmentDialogCategoriesBinding

    private val viewModel: MainViewModel by viewModels({ requireActivity() }) {
        InjectorUtils.provideMainViewModelFactory()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDialogCategoriesBinding.inflate(inflater, container, false)
        val categoryAdapter = CategoryAdapter()
        categoryAdapter.onChangeListener = viewModel::selectCategory
        binding.categoryList.adapter = categoryAdapter
        binding.clear.setOnClickListener {
            viewModel.deselectAllCategories()
            dismiss()
        }

        viewModel.getCategories().observe(viewLifecycleOwner, Observer { result ->
            binding.progress.hide()
            if (result.isSuccess) {
                categoryAdapter.submitList(result.getOrDefault(listOf()))
            } else {
                dismiss()
            }
        })

        return binding.root
    }
}